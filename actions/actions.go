package actions

type Action int

const (
	StartContainer = iota
	StopContainer
	ExecContainer
	RemoveContainer
	GetContainers
	GetContainerWithSameName
	GetContainerLog
	PullImage
	GetConfig
	Ping
	Test
	TestError
)
