module codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2

go 1.19

require github.com/docker/docker v20.10.17+incompatible

require (
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.5.0 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
)
