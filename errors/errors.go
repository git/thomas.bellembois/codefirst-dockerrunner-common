package errors

type Type int

const (
	DockerPull = iota
	CreateContainer
	StopContainer
	StartContainer
	ExecContainer
	RemoveContainer
	GetContainers
	WaitContainer
	GetContainerLog
	Server
	ImageURLValidation
	ImageNameValidation
	MaxContainersReached
	NonExistingContainer
)

type AppError struct {
	Type               Type   `json:"type"`
	SourceErrorMessage string `json:"sourceerrormessage"`
	Message            string `json:"message,omitempty"`
}

func (e AppError) String() string {
	errorString := ""

	switch e.Type {
	case DockerPull:
		errorString = "Error pulling the image."
	case CreateContainer:
		errorString = "Error creating the container."
	case StopContainer:
		errorString = "Error stopping the container."
	case StartContainer:
		errorString = "Error starting the container."
	case ExecContainer:
		errorString = "Error executing command in the container."
	case RemoveContainer:
		errorString = "Error removing the container."
	case GetContainers:
		errorString = "Error retrieving the running containers."
	case WaitContainer:
		errorString = "Error waiting for the started container."
	case GetContainerLog:
		errorString = "Error getting the container log."
	case Server:
		errorString = "Internal server error."
	case ImageNameValidation:
		errorString = "Docker image name invalid."
	case ImageURLValidation:
		errorString = "Docker image URL invalid."
	case MaxContainersReached:
		errorString = "Maximum number of containers reached."
	case NonExistingContainer:
		errorString = "Non existing container."
	default:
		errorString = "Unexpected error"
	}

	return errorString
}
