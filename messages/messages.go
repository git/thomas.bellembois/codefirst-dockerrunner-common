package messages

import (
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/errors"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/models"
	"github.com/docker/docker/api/types"
)

type WSMessage struct {
	Action     int                         `json:"action,omitempty"`
	Container  models.CodeFirstContainer   `json:"container,omitempty"`
	Containers []models.CodeFirstContainer `json:"containers,omitempty"`
	Config     models.CodefirstConfig      `json:"config,omitempty"`
	Exec       types.ExecConfig            `json:"exec,omitempty"`
	Message    string                      `json:"message,omitempty"`
	Error      *errors.AppError            `json:"error,omitempty"`
}
