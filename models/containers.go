package models

type CodeFirstContainer struct {
	ID                string   `json:"id"`
	Name              string   `json:"name"`
	ImageURL          string   `json:"imageurl"`
	EndpointURL       string   `json:"endpointurl"`
	Base64Credentials string   `json:"base64credentials,omitempty"`
	Env               []string `json:"env,omitempty"`
	Private           bool     `json:"private,omitempty"`
	Overwrite         bool     `json:"overwrite,omitempty"`
	IsStarting        bool     `json:"isstarting,omitempty"`
	IsDeleting        bool     `json:"isdeleting,omitempty"`
}
