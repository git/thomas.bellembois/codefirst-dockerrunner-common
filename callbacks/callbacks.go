package callbacks

type Callback int

const (
	BeforeStartContainerCallback = iota
	AfterStartContainerCallback
	BeforeCreateContainerCallback
	AfterCreateContainerCallback
	BeforeExecContainerCallback
	AfterExecContainerCallback
	BeforeStopContainerCallback
	AfterStopContainerCallback
	BeforeRemoveContainerCallback
	AfterRemoveContainerCallback
	BeforeGetContainersCallback
	AfterGetContainersCallback
	BeforeGetContainerWithSameNameCallback
	AfterGetContainerWithSameNameCallback
	BeforeGetContainerLogCallback
	AfterGetContainerLogCallback
	BeforePullImageCallback
	AfterPullImageCallback
	BeforeGetConfigCallback
	AfterGetConfigCallback
	BeforePingCallback
	AfterPingCallback
	BeforeTestCallback
	AfterTestCallback
	BeforeTestErrorCallback
	AfterTestErrorCallback
	Log
)
